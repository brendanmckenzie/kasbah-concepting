# Kasbah default content

## Base Values

The contents of this folder has no hierarchical relevance.  Each .yml file will be processed regardless of its position in the filesystem.

The files represent the default values to use when creating new nodes of the type defined in the file.

All content based on the type defined in the file will start with the values from the file and will be overwritten by what's defined in the database.

For example, if a content type, based on the `ContentPage` type, has a Base Values file that defines the `Title` field value of "Hello world", all content based on `ContentPage` will have that value for `Title` unless another value has been changed in the database.

## Static Tree

This folder structure represents the content tree that will be reflected when the application starts.
